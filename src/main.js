import Vue from 'vue'
import App from './App.vue'
import './assets/css/reset.css'
import './assets/css/application.css'
import './assets/css/fontawesome.min.css'
import './assets/css/regular.min.css'
import './assets/css/solid.min.css'
import './assets/css/layout.css'
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
